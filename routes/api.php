<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function(){
	Route::get('getGasolines', 'Api\GasolineApiController@index');
	Route::get('getStates', 'Api\GasolineApiController@getStates');
	Route::post('getTown', 'Api\GasolineApiController@getTown');
	Route::get('zipcodes', 'Api\GasolineApiController@getZipcode');
	Route::get('gasolines', 'Api\GasolineApiController@getGasoline');
});
