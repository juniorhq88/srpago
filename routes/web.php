<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'root', 'uses' => 'HomeController@index']);
Route::get('csv_file', 'HomeController@csv_index')->name('csv');
Route::post('csv_file/import', 'HomeController@csv_import')->name('import');
