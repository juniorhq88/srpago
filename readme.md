# Proyecto Laravel - SrPago

Proyecto de Laravel en su version 5.8 que permite consumir una la API para mostrar las ubicaciones de estaciones de gasolina en México.

## Instrucciones para instalar la aplicación

```sh
$ composer install
```

Copiamos el .env.example a .env donde pondremos los datos de accesos y conexión a la BD

```sh
$ cp .env.example .env
$ instalamos el archivo sql ubicado en /sql/srpago.sql 
$ y en el archivo .env ponemos las credenciales necesarias

$ npm i
$ npm run watch (para compilar y minificar los archivos sass 
$ y js en public y mantener el node en espera de nuevos cambios)
$ npm run dev (para compilar y minificar los archivos sass y js en public)
$ npm run prod (para compilar y minificar los archivos sass y 
$ js en public en producción)
$ php artisan serve (para levantar el servidor local)
```

## Autor

* **Junior Hernandez** - [Email](mailto:juniorhq88@gmail.com)