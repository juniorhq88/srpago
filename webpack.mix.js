let mix = require('laravel-mix'),
		assetsDir   = 'resources/',
	    nodeDir     = 'node_modules/',
	    adminLteDir = nodeDir + 'admin-lte/',
	    publicDir   = 'public/',
	    distDir     = 'public/dist/',
	    composerDir = 'vendor/',

	    adminJs = [
	    // nodeDir + 'jquery/dist/jquery.min.js',
	    // nodeDir + 'popper/dist/popper.min.js',
	    nodeDir + 'bootstrap/dist/bootstrap.min.js',

	    adminLteDir + 'plugins/jquery-ui/jquery-ui.min.js',
	    adminLteDir + 'plugins/bootstrap/js/bootstrap.bundle.min.js',
	    adminLteDir + 'plugins/chart.js/Chart.min.js',
	    adminLteDir + 'plugins/sparklines/sparkline.js',
	    adminLteDir + 'plugins/jqvmap/jquery.vmap.min.js',
	    adminLteDir + 'plugins/jqvmap/maps/jquery.vmap.world.js',
	    adminLteDir + 'plugins/moment/moment.min.js',
	    adminLteDir + 'plugins/daterangepicker/daterangepicker.js',
	    adminLteDir + 'plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js',
	    adminLteDir + 'plugins/summernote/summernote-bs4.min.js',
	    adminLteDir + 'plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js',
	    adminLteDir + 'plugins/fastclick/fastclick.js',
	    adminLteDir + 'plugins/datatables/jquery.dataTables.min.js',
	    adminLteDir + 'plugins/datatables/dataTables.bootstrap4.js',
	    adminLteDir + 'dist/js/adminlte.js',
	    //assetsDir  + 'js/custom.js'
	    ];

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
	.combine(
    adminJs
   // 'resources/assets/js/custom.js',
  ,'public/js/plugin.js')
    .sass('resources/sass/app.scss', 'public/css')
    .browserSync('http://localhost:8000')
    .disableNotifications();

if (mix.inProduction()) {
    mix.version();
}
