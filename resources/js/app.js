require('./bootstrap');
var srPago = srPago || {};

srPago.pageLoad =
{
	elementLoad:function()
  	{
  		srPago.events.selections();
  	}
};

srPago.events = 
{

	getGasolines: function() {

		var order = 0;
		var columns = [];
		var columnDefs = [];
			renderLinkEdit = function(a, b, c, d){
				return a;
			}

			columns = [];
			columnDefs.push({ targets: [0], orderable: false });

		columns = columns.concat([
			{data: 'codigopostal'},
			{data: 'calle'},
			{data: 'razonsocial'},
			{data: 'estado'},
			{data: 'municipio'},
			{data: 'regular'},
			{data: 'premium'},
		]);

		var oTable = $('.new-table').DataTable({
			processing: true,
			serverSide: false,
			pageLength: 100,
	        responsive: true,
			columns: columns,
			columnDefs: columnDefs,
			order: [[ order, 'desc' ]],
			async: true,
			deferRender: true,
			"deferLoading": 57,
			ajax: {
				'url': BASE_URL + '/api/v1/getGasolines',
				'method': 'GET',
				error: function( d ){
					console.log( d.success );
				}
			}
		});
},

	getStates: function(){
		var states = srPago.utils.getUrl(BASE_URL + '/api/v1/getStates');
			states.then(res => {
				var html = '';
				if(res.success === true){
					$.each(res.results, function(key, value) {
						html += '<option value="' + key + '" data-value="' + value.normalize('NFD').replace(/[\u0300-\u036f]/g,"") + '" class="filter">' + value + '</option>';
					});
					
					$('select[name="states"]').append(html);
					$('select[name="town"]').prop('disabled',false);
                     // console.log(res.results);
                    }
			}).catch(error => {
				console.log(error);
			});
	},
	getTown: function(){
		var state = $('select[name="states"]').children("option:selected").val();
		$('select[name="town"]').html('<option value="">-- Seleccione un Municipio --</option>');
		$.post(BASE_URL + '/api/v1/getTown',{state: state}).then(res => {
			var html = '';
			html += '<option value="" data-value="">-- Seleccione un Municipio --</option>';
			if(res.success === true){
				$.each(res.results, function(key, value) {
						html += '<option value="' + $.trim(value) + '" data-value="' + $.trim(value.normalize('NFD').replace(/[\u0300-\u036f]/g,"")) + '" class="filter">' + value + '</option>';
					});
				$('select[name="town"]').html(html);
			}
		}).catch(error => {
			console.log(error);
		})
	},
	selections: function (){
		$('select[name="states"]').change(function(){
			srPago.events.getTown();
		});		
		
		$('select[name="states"]').change(function(){
			var table = $('.new-table').DataTable();
			srPago.events.sortByStates(table);
		});

		$('select[name="town"]').change(function(){
			var table = $('.new-table').DataTable();
			srPago.events.sortByTowns(table);
		});

		$('select[name="price"]').change(function(){
			var table = $('.new-table').DataTable();
			srPago.events.sortRegularPrice(table);
		});
		$('select[name="pricePremium"]').change(function(){
			var table = $('.new-table').DataTable();
			srPago.events.sortPremiumPrice(table);
		});
	},
	sortByStates:function (table) {
		var state = $('select[name="states"]').children("option:selected").data('value');

				table.columns(3).search(state).draw();

	},
	sortByTowns:function (table) {
		var town = $('select[name="town"]').children("option:selected").data('value');

			if(town == ''){
				var state = $('select[name="states"]').children("option:selected").data('value');
				table.columns(3).search(state).draw();
			}else{
				table.columns(4).search(town).draw();
			}
	},
	sortRegularPrice:function (table) {
		var price = $('select[name="price"]').children("option:selected").val();
		// console.log(price);
		
			if(price == 'max_price'){
				table.order([5,'desc']).draw();
			}else{
				table.order([5,'asc']).draw();
			}

	},
	sortPremiumPrice:function (table) {
		var price = $('select[name="pricePremium"]').children("option:selected").val();
		
			if(price == 'max_price'){
				table.order([6,'desc']).draw();
			}else{
				table.order([6,'asc']).draw();
			}
		
		
	}


};


srPago.utils = {
	getUrl: function(url){
		return new Promise(function(resolve, reject){
			var xhttp = new XMLHttpRequest();
			xhttp.open("GET", url, true);
			xhttp.onload = function(){
				if(xhttp.status == 200){
					resolve(JSON.parse(xhttp.response));
				}else{
					reject(xhttp.statusText);
				}
			}
			xhttp.onerror = function(){
				reject(xhttp.statusText);
			};
			xhttp.send();
		});
	},
	removeAccents: function(s)
	{
		var r=s.toLowerCase();
            r = r.replace(new RegExp(/\s/g),"");
            r = r.replace(new RegExp(/[àáâãäå]/g),"a");
            r = r.replace(new RegExp(/[èéêë]/g),"e");
            r = r.replace(new RegExp(/[ìíîï]/g),"i");
            r = r.replace(new RegExp(/ñ/g),"n");                
            r = r.replace(new RegExp(/[òóôõö]/g),"o");
            r = r.replace(new RegExp(/[ùúûü]/g),"u");
            
 		return r;
	}
};

$(document).ready(function()
{
  srPago.pageLoad.elementLoad();
});

window.onload = function () {
	srPago.events.getStates();
	srPago.events.getGasolines();
};