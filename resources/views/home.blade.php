@extends('layouts.master')
@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-6 col-12">
            <div class="form-group">
                <select name="states" class="form-control" id="states">
                    <option value="" data-value="">-- Seleccione un Estado --</option>
                </select>
            </div>
            <div class="form-group">
                <select name="town" class="form-control" id="town" disabled>
                    <option value="" data-value="">-- Seleccione un Municipio --</option>
                </select>
            </div>
            <div class="form-group">
                <select name="price" class="form-control" id="price">
                    <option value="">-- Ordenar Precio Regular --</option>
                    <option value="max_price">Precio Mayor</option>
                    <option value="min_price">Precio Menor</option>
                </select>
            </div>
            <div class="form-group">
                <select name="pricePremium" class="form-control" id="pricePremium">
                    <option value="">-- Ordenar Precio Premium --</option>
                    <option value="max_price">Precio Mayor</option>
                    <option value="min_price">Precio Menor</option>
                </select>
            </div>
        </div>
        <div class="col-lg-6 col-12">
            {!! $map['html'] !!}
        </div>
        <br><br>
        <div class="col-lg-12 col-12">
            <div class="table-responsive">
                <table class="table table-bordered table-hovered new-table">
                    <thead>
                        <tr>
                            <th>Codigo Postal</th>
                            <th>Dirección</th>
                            <th>Razón Social</th>
                            <th>Estado</th>
                            <th>Municipio</th>
                            <th>Precio Regular</th>
                            <th>Precio Premium</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection