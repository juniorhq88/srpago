<nav class="mt-2">
  <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
    <!-- Add icons to the links using the .nav-icon class
    with font-awesome or any other icon font library -->
    <li class="nav-item">
      <a href="{{ url('/') }}" class="nav-link {{ request()->is('/') ? 'active' : '' }}">
        <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>
          Dashboard
          <i class="right fas fa-angle-right"></i>
        </p>
      </a>
    </li>
    <li class="nav-item">
      <a href="{{ url('/csv_file') }}" class="nav-link {{ request()->is('/csv_file') ? 'active' : '' }}">
        <i class="nav-icon fas fa-tachometer-alt"></i>
        <p>
          CSV Import
          <i class="right fas fa-angle-right"></i>
        </p>
      </a>
    </li>
  </ul>
</nav>