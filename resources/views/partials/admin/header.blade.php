<section class="content-header">

    @php $route = Route::currentRouteName() @endphp

    <h1>
        {{  trans(Route::getCurrentRoute()->getName() == 'root' ? 'Dashboard' : trans('admin.'.Route::getCurrentRoute()->getName())) }}
        
    </h1>


</section>
