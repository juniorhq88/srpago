@extends('layouts.master')
@section('content')
<div class="container-fluid">
  <div class="row">
    <div class="col-lg-12 col-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          <h3 class="panel-title">Import Data in CSV File</h3>
        </div>
        <div class="panel-body">
          <form action="{{ route('import') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="file" name="file" accept=".csv">
            <br>
            <button class="btn btn-success">Import ZipCode Data</button>
          </form>
          @yield('csv_data')
        </div>
      </div>
    </div>
  </div>
</div>
@endsection