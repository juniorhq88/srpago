<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="_token" content="{{ csrf_token() }}" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title> Dashboard | Sr Pagos
         </title>
        <link rel="stylesheet" type="text/css" href="{{ asset(mix('css/app.css')) }}">
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}" type="image/x-icon" />
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        {!! $map['js'] !!}
    </head>
    <body class="hold-transition sidebar-mini">
        <div class="wrapper" id="app">
            @include('partials.admin.top')
            @include('partials.admin.sidebar')
            <div class="content-wrapper">
                
                @include('partials.admin.header')
                <section class="content">   

                            @yield('content')

                </section>
            </div>
            <footer class="main-footer">
                <div class="float-right d-none d-sm-block">
                    <b>Version</b> 3.0
                </div>
                <strong>Copyright &copy; 2019 All rights
                reserved.
            </footer>
        </div>
        <script src="{{ asset(mix('js/app.js')) }}" type="text/javascript"></script>
        <script src="{{ asset(mix('js/plugin.js')) }}" type="text/javascript"></script>
        
        <script>
        var BASE_URL = "{{ url('/') }}";
        var REQUEST_URL = "{{ Request::url() }}";
        var CSRF = "{{ csrf_token() }}";
        $.ajaxSetup({
        headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }
        });

        </script>
        @yield('script')
        <script>
        $.widget.bridge('uibutton', $.ui.button);
        </script>
        
    </body>
</html>