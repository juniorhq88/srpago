<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ZipCode extends Model
{
   protected $fillable = [
        'd_codigo',
         'd_asenta',
         'd_tipo_asenta',
         'd_mnpio',
         'd_estado',
         'd_ciudad'
    ];

    public $timestamps = false;
}
