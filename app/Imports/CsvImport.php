<?php

namespace App\Imports;

use App\ZipCode;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CsvImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $str = mb_convert_encoding($row["d_estado"], "UTF8", "JIS, eucjp-win, sjis-win");
        return new ZipCode([
            'd_codigo' => $row["d_codigo"],
            'd_asenta' => $row["d_asenta"],
            'd_tipo_asenta' => $row["d_tipo_asenta"],
            'd_mnpio' => $row["d_mnpio"],
            'd_estado' => $str,
            'd_ciudad' => $row["d_ciudad"],
            'd_cp' => $row["d_cp"],
            'c_estado' => $row["c_estado"],
            'c_oficina' => $row["c_oficina"],
            'c_cp' => $row["c_cp"],
            'c_tipo_asenta' => $row["c_tipo_asenta"],
            'c_mnpio' => $row["c_mnpio"],
            'id_asenta_cpcons' => $row["id_asenta_cpcons"],
            'd_zona' => $row["d_zona"],
            'c_cve_ciudad' => $row["c_cve_ciudad"]
        ]);
    }
}
