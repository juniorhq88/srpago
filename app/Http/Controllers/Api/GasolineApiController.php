<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\ZipCodeRepository;
use App\Repositories\StateRepository;
use App\Repositories\TownRepository;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use DataTables;

class GasolineApiController extends Controller
{
	private $repository;
    private $repoState;
    private $repoTown;

	function __construct(ZipCodeRepository $repository, StateRepository $repoState, TownRepository $repoTown)
	{
		$this->repository = $repository;
        $this->repoState = $repoState;
        $this->repoTown = $repoTown;
	}

    public function index()
    {

    	$gasolines = $this->getGasoline();

    	$gasArr = json_decode($gasolines, true);  	
    	

    	for ($i=0; $i < count($gasArr['results']); $i++) { 

    		$item = $gasArr['results'];

	    	$arg = array(
        		'codigo' => 'd_codigo',
        		'operator' => '=',
        		'value' => $item[$i]['codigopostal']
    		);

    		$addr = $this->searchByZipCode($arg);
              
    	    	$gasResult[] = array(
    	    		'_id' => $item[$i]['_id'],
    	    		'rfc' => $item[$i]['rfc'],
    	    		'razonsocial' => $item[$i]['razonsocial'],
    	    		'date_insert' => $item[$i]['date_insert'],
    	    		'numeropermiso' => $item[$i]['numeropermiso'],
    	    		'fechaaplicacion' => $item[$i]['fechaaplicacion'],
    	    		'permisoid' => $item[$i]['﻿permisoid'],
    	    		'longitude' => $item[$i]['longitude'],
    	    		'latitude' => $item[$i]['latitude'],
    	    		'codigopostal' => $item[$i]['codigopostal'],
    	    		'calle' => $item[$i]['calle'],
    	    		'colonia' => $item[$i]['colonia'],
    	    		'municipio' => $addr['d_mnpio'],
    	    		'estado' => $addr['d_estado'],
    	    		'regular' => '$ ' . $item[$i]['regular'],
    	    		'premium' => '$ ' . $item[$i]['premium'],
    	    		'dieasel' => $item[$i]['dieasel']
    	    	);

    	 }

          $collection = collect($gasResult);

        return DataTables::of($collection)->toJson();
    }


    public function getZipcode()
    {

    	$result = array(
    		'success' => true,
    		'results' => $this->repository->getPaginated(),
    		'total'  => $this->repository->getAll()->count()
    	);
    	return response()->json($result);
    }

    public function getGasoline()
    {
    	$client = new Client();

        $items = '10018';

    	$gasolineURL = 'https://api.datos.gob.mx/v1/precio.gasolina.publico?pageSize=500';

    	# Send an asynchronous request with promise.
		$promise = $client->getAsync($gasolineURL)->then(function ($response) {

            return $response->getBody();
            
		}, function ($exception) {
            return $exception->getMessage();
        });

		$response = $promise->wait();
        return $response;
    }

    public function getStates()
    {
        $states = $this->repoState->getAll()->pluck('name','id');

        $result = array(
            'success' => true,
            'results' => $states,
            'total'  => $states->count()
        );
        return response()->json($result);
    }

    public function getTown(Request $request)
    {
        $data = $request->all();

        $town = $this->repoTown->where('state_id','=',$data['state'])->pluck('name','id');

        $result = array(
            'success' => true,
            'results' => $town,
            'total'  => $town->count()
        );
        return response()->json($result);
    }

    public function searchByZipCode(array $val)
    {

    	$data = $val;

    	$result = $this->repository->where($data['codigo'],$data['operator'],$data['value'])->first();

    	return $result;

    }
}
