<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Imports\CsvImport;
use Maatwebsite\Excel\Facades\Excel;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use GMaps;

class HomeController extends Controller
{
    public function index()
    {
        $config = array();
        $config['center'] = '24.2577475,-103.1734771';
        $config['zoom'] = '5';
        $config['map_height'] = '100%';
        $config['scroll_wheel'] = false;

        GMaps::initialize($config);

        $marker = array();

        $gasolines = $this->getGasoline();

        $gasArr = json_decode($gasolines, true);    
        

        for ($i=0; $i < count($gasArr['results']); $i++) { 

            $item = $gasArr['results'][$i];
           // Add marker
            $marker['position'] = $item['latitude'].','.$item['longitude'];
            $pregular = $item['regular'] != '' ? 'Precio Regular: '.$item['regular'] : '';
            $ppremium = $item['premium'] != '' ? 'Precio Premium: '.$item['premium'] : '';
            $marker['infowindow_content'] = $item['calle'].'<br>'.$pregular.'<br>'.$ppremium;
            // $marker['icon'] = url('/images/ping.png');
            GMaps::add_marker($marker);
        }
        

        $map = GMaps::create_map();

    	return view('home', compact('map'));
    }

    public function csv_import()
    {
    	Excel::import(new CsvImport, request()->file('file'));
    	return back();
    }

    public function csv_index()
    {
    	return view('csv_files');
    }

    public function getGasoline()
    {
        $client = new Client();
        $gasolineURL = 'https://api.datos.gob.mx/v1/precio.gasolina.publico?pageSize=350';

        # Send an asynchronous request with promise.
        $promise = $client->getAsync($gasolineURL)->then(function ($response) {

            return $response->getBody();
            
        }, function ($exception) {
            return $exception->getMessage();
        });

        $response = $promise->wait();
        return $response;
    }

}
