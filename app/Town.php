<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Town extends Model
{
      protected $fillable = [
        'state_id',
         'name'
    ];

    public $timestamps = false;
}
