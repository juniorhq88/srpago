<?php

namespace App\Repositories;

use App\State;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class StateRepository implements UserRepositoryInterface
{
    protected $model;

    /**
     * UserRepository constructor.
     *
     * @param State $state
     */
    public function __construct(State $state)
    {
        $this->model = $state;
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function getPaginated()
    {
        return $this->model->paginate(10);
    }


    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function update(array $data, $id)
    {
        return $this->model->find($id)
            ->first()
            ->update($data);
    }

    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    public function find($id)
    {
        if (null == $state = $this->model->find($id)) {
            throw new ModelNotFoundException("State not found");
        }

        return $state;
    }
}