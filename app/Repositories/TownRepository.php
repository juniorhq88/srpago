<?php

namespace App\Repositories;

use App\Town;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class TownRepository implements UserRepositoryInterface
{
    protected $model;

    /**
     * UserRepository constructor.
     *
     * @param Town $town
     */
    public function __construct(Town $town)
    {
        $this->model = $town;
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function getPaginated()
    {
        return $this->model->paginate(10);
    }


    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function update(array $data, $id)
    {
        return $this->model->find($id)
            ->first()
            ->update($data);
    }

    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    public function find($id)
    {
        if (null == $town = $this->model->find($id)) {
            throw new ModelNotFoundException("State not found");
        }

        return $town;
    }

    public function where($field,$operator = '=',$value)
    {
        return $this->model->where($field,$operator,$value);
    }
}