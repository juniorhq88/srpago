<?php

namespace App\Repositories;

use App\ZipCode;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ZipCodeRepository implements ZipCodeRepositoryInterface
{
    protected $model;

    /**
     * ZipCodeRepository constructor.
     *
     * @param ZipCode $zipcode
     */
    public function __construct(ZipCode $zipcode)
    {
        $this->model = $zipcode;
    }

    public function getAll()
    {
        return $this->model->all();
    }

    public function getPaginated()
    {
        return $this->model->paginate(10);
    }


    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function update(array $data, $id)
    {
        return $this->model->find($id)
            ->first()
            ->update($data);
    }

    public function delete($id)
    {
        return $this->model->destroy($id);
    }


    public function find($id)
    {
        if (null == $zipcode = $this->model->find($id)) {
            throw new ModelNotFoundException("ZipCode not found");
        }

        return $zipcode;
    }

    public function where($field,$operator = '=',$value)
    {
        return $this->model->where($field,$operator,$value);
    }

}